export class Ticker {

    public pair: string;
    public values: TickerVals;
}

export class TickerVals {

  public id: number;
  public last: number;
  public lowestAsk: number;
  public highestBid: number;
  public percentChange: number;
  public baseVolume: number;
  public quoteVolume: number;
  public isFrozen: number;
  public high24hr: number;
  public low24hr: number;
}

export class TickerList {
  public tickers: Ticker[];
}

export class TickerForTable {

  public pair: string;
  public id: number;
  public last: number;
  public lowestAsk: number;
  public highestBid: number;
  public percentChange: number;
  public baseVolume: number;
  public quoteVolume: number;
  public isFrozen: number;
  public high24hr: number;
  public low24hr: number;

  constructor (pair: string, id: number, last: number, lowestAsk: number, highestBid: number, percentChange: number,
               baseVolume: number, quoteVolume: number, isFrozen: number, high24hr: number, low24hr: number) {
    this.pair = pair;
    this.id = id;
    this.last = last;
    this.lowestAsk = lowestAsk;
    this.highestBid = highestBid;
    this.percentChange = percentChange;
    this.baseVolume = baseVolume;
    this.quoteVolume = quoteVolume;
    this.isFrozen = isFrozen;
    this.high24hr = high24hr;
    this.low24hr = low24hr;
  };
}
