import {Component, NgModule, OnInit, ViewEncapsulation} from '@angular/core';
import {TickerVals} from './Ticker';
import {Observable} from 'rxjs/Rx';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BasetableComponent} from '../basetable/basetable.component';
import {StockService} from '../stock.service';
import {CommonService} from '../common.service';


@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [StockService, CommonService]
})

@NgModule({
  declarations: [],
  imports: [NgbModule]
})

export class TickerComponent extends BasetableComponent {

  selectedRow: Number;
  setClickedRow: Function;
  pair: String;

  public pairs = ['ALL', 'BTC', 'ETH', 'XMR', 'USDT'];
  public pairsDescr = ['All pairs', 'Bitcoin', 'Ethereum', 'Monero', 'Tether'];

  private timer: Observable<number>;

  public columns = ['#', 'Pair', 'ID', 'Last', 'Lowest Ask', 'Highest Bid', 'Percent Change', 'Base Volume',
    'Quote Volume', 'Is Frozen', 'High 24hr', 'Low 24hr'];
  tickers: TickerVals[];
  keys: string[];

  constructor(stockService: StockService, common: CommonService) {
    super(stockService, common);
  }

  public getData(): Observable<Object> {
    console.log('1111111111111');
    return this.stockService.getOrderBook('USDT_BTC', 10);
  }

  ngOnInit() {
    console.log('INIT 1111');
    super.ngOnInit();
    console.log('INIT 2222');
  }

}
