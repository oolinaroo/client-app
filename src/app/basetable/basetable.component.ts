import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {StockService} from '../stock.service';
import {CommonService} from '../common.service';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-basetable',
  templateUrl: './basetable.component.html',
  styleUrls: ['./basetable.component.css'],
  providers: [StockService, CommonService]
})
export class BasetableComponent implements OnInit, OnDestroy {

  @Input() enablePolling: boolean;
  @Input() timeOut: number;

  public data: any = null;
  private sub: Subscription;
  public columns: string[];

  constructor(protected stockService: StockService, protected common: CommonService) {
    if (isNullOrUndefined(this.enablePolling)) {
      this.enablePolling = false;
    }
    if (isNullOrUndefined(this.timeOut)) {
      this.timeOut = 2000;
    }
  }

  ngOnInit() {
    console.log('BASE ngOnInit 1111');
    if (this.enablePolling) {
      this.startPolling();
    } else {
      this.getData()
        .subscribe((response) => {
          this.data = response;
        });
    }
    console.log('BASE ngOnInit 2222');
  }

  ngOnDestroy() {
    if (this.enablePolling) {
      this.endPolling();
    }
  }

  public getData(): Observable<Object> {
    console.log('BASE getData');
    return null;
  }

  protected startPolling() {
    this.sub = Observable.interval(this.timeOut)
      .concatMap(() => this.getData())
      .subscribe((response) => {
        this.data = response;
      });
  }

  protected endPolling() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
