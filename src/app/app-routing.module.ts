import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TickerComponent} from './ticker/ticker.component';
import {OrderComponent} from './order/order.component';
import {WidgetComponent} from './widget/widget.component';
import {TickerorderComponent} from './tickerorder/tickerorder.component';
import {ChartComponent} from './chart/chart.component';
import {TradehistoryComponent} from './tradehistory/tradehistory.component';
import {TickerorderdetailComponent} from './tickerorderdetail/tickerorderdetail.component';
import {TradehistorynewComponent} from './tradehistorynew/tradehistorynew.component';
import {TraidingComponent} from './traiding/traiding.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: WidgetComponent},
  {path: 'orders', component: OrderComponent},
  {path: 'tickers', component: TickerComponent},
  {path: 'tickerorders', component: TickerorderComponent},
  {path: 'charts', component: ChartComponent},
  {path: 'tradehistory', component: TradehistoryComponent},
  {path: 'tickerordersdetail', component: TickerorderdetailComponent},
  {path: 'tradehistorynew', component: TradehistorynewComponent},
  {path: 'traiding', component: TraidingComponent},
  {path: 'test', component: TestComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
