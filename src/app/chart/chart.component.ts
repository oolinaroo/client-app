import {Component, OnDestroy, OnInit} from '@angular/core';
import {StockService} from '../stock.service';
import {Observable} from 'rxjs/Observable';
import {ViewChild, Input} from '@angular/core';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';
import {CommonService} from '../common.service';
import {BasetableComponent} from '../basetable/basetable.component';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  providers: [StockService, CommonService]
})

export class ChartComponent extends BasetableComponent implements OnInit {

  @ViewChild('chart')
  chart: BaseChartDirective;

  @Input() pair: string;

  private period: number = null;
  private startDate: number = null;

  public btnZoom = {
    label: ['1h', '6h', '24h', '2d', '4d', '1w', '2w', '1m', 'Year'],
    value: [this.common.hour, 6 * this.common.hour, this.common.day, 2 * this.common.day,
      4 * this.common.day, this.common.week, 2 * this.common.week, 4 * this.common.week, 365 * this.common.day]
  };
  public btnCandlesticks = {
    label: ['5-min', '15-min', '30-min', '2-hr', '4-hr', '1-day'],
    value: [5 * this.common.min, 15 * this.common.min, 30 * this.common.min, 2 * this.common.hour, 4 * this.common.hour, this.common.day]
  };

  private chartdata;
  public d: Array<any> = [
    {data: [], label: 'high', fill: false},
    {data: [], label: 'low', fill: false},
    {data: [], label: 'weightedAverage', fill: false}];
  public lineChartLabels: Array<any> = [];
  private endDate: number;
  public options: any = {
    responsive: true,
    title: {
      display: true,
      text: null,
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Time'
        },
        type: 'time',
        time: {
          // unit: 'minute'
          displayFormats: {
            'minute': 'DD MMM HH:mm',
            'hour': 'DD MMM HH:mm'
          },
        }
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Price'
        } /* ,
        ticks: {
          min: 10500,
          max: 11000,

          // forces step size to be 5 units
          stepSize: 100
        } */

      }]
    }
  }

  constructor(stockService: StockService, common: CommonService) {
    super(stockService, common);
  }

  ngOnInit() {
    console.log('CHART ngOnInit 1111');
    this.initChart();
    super.ngOnInit();
    console.log('CHART ngOnInit 2222');
  }

  public getData(): Observable<Object> {
    let response;
    if (isNullOrUndefined(this.endDate)) {
      response = this.stockService.getChartData(this.pair, this.startDate, this.period);
      this.processingInitData();
    } else {
      response = this.stockService.getChartData(this.pair, this.endDate, this.period);
      this.processingData();
    }
    // console.log(JSON.stringify(response))
    return response;
  }

  private initChart() {
    if (isNullOrUndefined(this.startDate)) {
      this.startDate = this.common.getSecondsAgo(this.common.getCookie('.Chart.Zoom', this.common.getHoursAgo(24)));
    }
    if (isNullOrUndefined(this.period)) {
      this.period = this.common.getCookie<number>('.Chart.Candlesticks', this.common.getMinutesAgo(5));
    }
    this.options.title.text = this.pair;
  }

  private popData(firstData: Array<any>, ...datas: Array<any>[]) {
    firstData.pop();
    for (let i = 0; i < datas.length; i++) {
      datas[i].pop();
    }
  }

  private processingInitData() {
    console.log(JSON.stringify(this.data));
    if (isNullOrUndefined(this.data)) {
      return;
    }
    this.chartdata = this.data;
    this.lineChartLabels = [];
    this.d[0].data = [];
    this.d[1].data = [];
    this.d[2].data = [];
    this.chartdata.forEach(item => {
      this.d[0].data.push(item.high);
      this.d[1].data.push(item.low);
      this.d[2].data.push(item.weightedAverage);
      this.endDate = item.date;
      console.log(this.endDate );
      this.lineChartLabels.push(new Date(this.endDate * 1000)/*.toLocaleTimeString('ru-RU')*/);
    });
    this.data = this.data;
  }

  private processingData() {
    console.log(JSON.stringify(this.data));
    if (isNullOrUndefined(this.data)) {
      return;
    }
    this.chartdata = this.data;
    this.chartdata.forEach(item => {
      this.popData(
        this.chart.chart.data.datasets[0].data,
        this.chart.chart.data.datasets[1].data,
        this.chart.chart.data.datasets[2].data,
        this.chart.chart.data.labels);
    });
    this.chartdata.forEach(item => {
      this.chart.chart.data.labels.push(new Date(this.endDate * 1000)/*.toLocaleTimeString('ru-RU')*/);
      this.chart.chart.data.datasets[0].data.push(item.high);
      this.chart.chart.data.datasets[1].data.push(item.low);
      this.chart.chart.data.datasets[2].data.push(item.weightedAverage);
      this.endDate = item.date;
    });
    // this.chart.chart.update();
  }

  private onChangeZoom(value: number) {
    // this.endPolling();
    this.startDate = this.endDate - value;
    this.common.putCookie<number>('.Chart.Zoom', value);
    // this.initChart();
    // this.startPolling();
    console.log(value);
  }

  private onChangeCandlesticks(value: number) {
    // this.endPolling();
    this.period = value;
    this.common.putCookie<number>('.Chart.Candlesticks', value);
    // this.initChart();
    // this.startPolling();
    console.log(value);
  }
}
