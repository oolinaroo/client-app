import {Component, NgModule, OnInit, ViewEncapsulation} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {OrderVals} from './Order';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {

  private timer: Observable<number>;

  public captions = ['Sell', 'Buy']
  public columns = ['#', 'Price', 'Volume', 'Price * Volume'];
  public arrs = ['Asks', 'Bids'];
  asks: OrderVals[];
  bids: OrderVals[];
  keys: string[];

  constructor(private http: HttpClient) {
  }

  getOrders() {
    /*this.http.get(environment.apiEndpoint + '/returnOrderBook/USDT_BTC/10', {responseType: 'json'})
      .subscribe(data => {
        this.keys = Object.keys(data);
        Object.keys(data).map((key) => {
          switch (key) {
            case 'asks':
              this.asks = data[key];
              break;
            case 'bids':
              this.bids = data[key];
              break;
          }
        });
        // console.log(this.asks);
        // console.log(this.bids);
      });*/
  }

  getAskVolume(index: number): number {
    let volume = 0;
    volume = this.asks[index][0] * this.asks[index][1]
    return volume;
  }

  getBidVolume(index: number): number {
    let volume = 0;
    volume = this.bids[index][0] * this.bids[index][1]
    return volume;
  }

  ngOnInit() {
    // this.getOrders();
    this.timer = Observable.timer(5000, 500);
    this.timer.subscribe(t => this.getOrders());
  }

  onclick() {
    // this.getOrders();
  }
}
