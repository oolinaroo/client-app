import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerorderdetailComponent } from './tickerorderdetail.component';

describe('TickerorderdetailComponent', () => {
  let component: TickerorderdetailComponent;
  let fixture: ComponentFixture<TickerorderdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TickerorderdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerorderdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
