import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {StockService} from '../stock.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-tickerorderdetail',
  templateUrl: './tickerorderdetail.component.html',
  styleUrls: ['./tickerorderdetail.component.css'],
  providers: [StockService]
})

export class TickerorderdetailComponent implements OnInit, OnDestroy {

  @Input() pair: string;

  public orderbook;
  private sub: Subscription;
  public captions = ['Sell', 'Buy']
  public columns = ['#', 'Price', 'Volume', 'Price * Volume'];
  public totalAsk: any;
  public totalBid: any;

  constructor(private stockService: StockService) {
    this.totalAsk = [0, 0, 0];
    this.totalBid = this.totalAsk;
  }

  ngOnInit() {
    this.startPolling();
  }

  ngOnDestroy() {
    this.endPolling();
  }

  private endPolling() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  private startPolling() {
    this.sub = Observable.interval(100)
      .concatMap(() => this.stockService.getOrderBook(this.pair, 15))
      .subscribe((data) => {
        this.orderbook = data;
        this.getTotals();
        // console.log(this.totalAsk);
        // console.log(data);
      });
  }

  getTotals() {
    const data = [0, 0, 0];
    this.totalAsk = this.orderbook.asks.reduce((prev: any, curr: any) => {
      return [curr[0], prev[1] + curr[1], Number(prev[0]) * prev[1] + Number(curr[0]) * curr[1]];
    }, data);
    this.totalBid = this.orderbook.bids.reduce((prev: any, curr: any) => {
      return [curr[0], prev[1] + curr[1], Number(prev[0]) * prev[1] + Number(curr[0]) * curr[1]];
    }, data);
  }

}
