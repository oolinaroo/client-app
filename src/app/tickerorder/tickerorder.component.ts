import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {StockService} from '../stock.service';
import {Observable} from 'rxjs/Observable';
import {BasetableComponent} from '../basetable/basetable.component';
import {CommonService} from '../common.service';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-tickerorder',
  templateUrl: './tickerorder.component.html',
  styleUrls: ['./tickerorder.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [StockService, CommonService]
})

export class TickerorderComponent extends BasetableComponent implements OnInit {

  private BTC: any;
  private ETH: any;
  private XMR: any;
  private USDC: any;

  private selectedPair: string;
  private depth: number;
  public searchText: string;

  constructor(stockService: StockService, common: CommonService) {
    super(stockService, common);
    this.columns = ['#', 'Icon', 'Name', 'Last Ask', 'in USDT', 'Last Bid', 'in USDT', 'Ask-Bid', 'Ask Volume', 'Ask Total',
      'Bid Volume', 'Bid Total', 'isFrozen', 'Seq'];
    this.enablePolling = true;
    this.depth = 3;
  }

  ngOnInit() {
    super.ngOnInit();
    this.selectedPair = null;
  }

  public getData(): Observable<Object> {
    // console.log('TickerOrder');
    // return this.stockService.getOrderBook('ALL', this.depth);
    const response = this.stockService.getStatisticOrderBook('ALL', 10);
    this.updateLastRates();
    return response;
  }

  private getChr(value: string): string {
    // console.log(value.substring(value.indexOf('_') + 1));
    return value.substring(value.indexOf('_') + 1);
  }

  private setPair(pair: string) {
    if (pair !== this.selectedPair) {
      this.selectedPair = pair;
      // this.endPolling();
    } else {
      this.selectedPair = null;
      // this.startPolling();
    }
  }

  private getRate(value: any) {
    return {ask: value.asks.lastPrice, bid: value.bids.lastPrice};
  }

  public updateLastRates() {
    if (isNullOrUndefined(this.data)) {
      return;
    }
    this.data.forEach((value: any) => {
      switch (value.key) {
        case 'USDT_BTC':
          this.BTC = this.getRate(value);
          break;
        case 'USDT_ETH':
          this.ETH = this.getRate(value);
          break;
        case 'USDT_XMR':
          this.XMR = this.getRate(value);
          break;
        case 'USDC_USDT':
          this.USDC = this.getRate(value);
          break;
      }
    });
  }

  public getInUSDT(pair: string, type: boolean, price: number): number {
    return this.common.getPriceInUSDT(pair, type, price, this.BTC, this.ETH, this.XMR, this.USDC);
  }

}
