import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerorderComponent } from './tickerorder.component';

describe('TickerorderComponent', () => {
  let component: TickerorderComponent;
  let fixture: ComponentFixture<TickerorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TickerorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
