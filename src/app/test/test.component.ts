import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BasetableComponent} from '../basetable/basetable.component';
import {StockService} from '../stock.service';
import {CommonService} from '../common.service';
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  providers: [StockService, CommonService]
})
export class TestComponent extends BasetableComponent {

  BTC: any;
  ETH: any;
  XMR: any;
  USDC: any;

  constructor(stockService: StockService, common: CommonService) {
    super(stockService, common);
    this.enablePolling = true;
  }

  public getData(): Observable<Object> {
    console.log('TestComponent');
    const response = this.stockService.getStatisticOrderBook('ALL', 10);
    this.updateLastRates();
    return response;
  }

  private getRate(value: any) {
    return {ask: Number(value.asks.lastPrice), bid: Number(value.bids.lastPrice)};
  }

  public updateLastRates() {
    if (isNullOrUndefined(this.data)) {
      return;
    }
    this.data.forEach((value: any) => {
      switch (value.key) {
        case 'USDT_BTC':
          this.BTC = this.getRate(value);
          break;
        case 'USDT_ETH':
          this.ETH = this.getRate(value);
          break;
        case 'USDT_XMR':
          this.XMR = this.getRate(value);
          break;
        case 'USDC_USDT':
          this.USDC = this.getRate(value);
          break;
      }
    });
  }

  public getTest(pair: string, type: boolean, price: number): number {
    return this.common.getPriceInUSDT(pair, type, price, this.BTC, this.ETH, this.XMR, this.USDC);
  }
}
