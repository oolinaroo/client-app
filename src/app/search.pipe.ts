import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchfilter'
})

export class SearchFilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toUpperCase();
    return items.filter((item: any) => {
      // console.log(item);
      return item.key.includes(searchText);
    });
  }
}
