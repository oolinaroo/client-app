import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {StockService} from '../stock.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-tradehistory',
  templateUrl: './tradehistory.component.html',
  styleUrls: ['./tradehistory.component.css'],
  providers: [StockService, CommonService]
})
export class TradehistoryComponent implements OnInit, OnDestroy {

  @Input() pair: string;
  @Input() limit: number = null;
  @Input() startDate: number = null;

  const
  maxValue = Number.MAX_SAFE_INTEGER;
  minValue = Number.MIN_SAFE_INTEGER;
  time = 300; // seconds

  public tradehistory = {
    sell0: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy0: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell1: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy1: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell2: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy2: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell3: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy3: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell4: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy4: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell5: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy5: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell6: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy6: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    sell7: {min: this.maxValue, max: this.minValue, amount: 0, count: 0},
    buy7: {min: this.maxValue, max: this.minValue, amount: 0, count: 0}
  };
  private sub: Subscription;
  public columns = ['#', 'Global Trade ID', 'Trade ID', 'Date', 'Type', 'Rate', 'Amount', 'Total'];
  private selectedPair: string;
  public parts = [5000, 10000, 15000, 20000, 25000, 30000, 60000, 120000]; // milliseconds
  public sellTotal = {percent: 0, min: 0, max: 0, amount: 0, count: 0};
  public buyTotal = {percent: 0, min: 0, max: 0, amount: 0, count: 0};

  constructor(private stockService: StockService, private common: CommonService) {
  }

  ngOnInit() {
    if (this.startDate === null) {
      this.startDate = this.common.getSecondsAgo(this.time);
    }
    if (this.limit === null) {
      this.limit = 1000;
    }
    this.startPolling();
  }

  ngOnDestroy() {
    this.endPolling();
  }

  private startPolling() {
    this.sub = Observable.interval(500)
      .concatMap(() => this.stockService.getStatisticTradeHistory(this.pair,
        this.common.getSecondsAgo(this.time), 9999999999, this.limit, this.parts))
      .subscribe((data) => {
        this.tradehistory = data;
        this.sellTotal = this.getTotal('sell');
        this.buyTotal = this.getTotal('buy');
        // console.log(data);
      });
  }

  private endPolling() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  private getStatistic(): Object {
    return null;
  }

  public getTotal(name: string) {
    // console.log('ssssssssssss');
    let min = this.maxValue;
    let max = this.minValue;
    let count = 0;
    let amount = 0;
    this.parts.forEach((val, i) => {
      min = (min > this.tradehistory[`${name}${i}`].min ? this.tradehistory[`${name}${i}`].min : min);
      max = (max < this.tradehistory[`${name}${i}`].max ? this.tradehistory[`${name}${i}`].max : max);
      amount += this.tradehistory[`${name}${i}`].amount;
      count += this.tradehistory[`${name}${i}`].count;
    });
    if (min === this.maxValue) {
      return {percent: 0, min: 0, max: 0, amount: 0, count: 0};
    } else {
      return {percent: (max - min) / min * 100, min: min, max: max, amount: amount, count: count};
    }
  }

  public getPercent(name: string, i: number) {
    return (this.tradehistory[name + i].max - this.tradehistory[name + i].min) / this.tradehistory[name + i].min * 100;
  }

}
