import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraidingComponent } from './traiding.component';

describe('TraidingComponent', () => {
  let component: TraidingComponent;
  let fixture: ComponentFixture<TraidingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraidingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
