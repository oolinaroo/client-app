import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {StockService} from "../stock.service";
import {CommonService} from "../common.service";
import {BasetableComponent} from "../basetable/basetable.component";
import {Observable} from "rxjs/Observable";
import {noUndefined} from "@angular/compiler/src/util";

@Component({
  selector: 'app-traiding',
  templateUrl: './traiding.component.html',
  styleUrls: ['./traiding.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [StockService, CommonService]
})
export class TraidingComponent extends BasetableComponent implements OnInit {

  public rate: number;
  public amount: number;
  public total: number;

  constructor(stockService: StockService, common: CommonService) {
    super(stockService, common);
    this.enablePolling = false;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  public getData(): Observable<Object> {
    console.log('ddddd');
    return null;
  }

  public changeRate() {
    // console.log('value ' + this.rate);
    if (!isNaN(this.rate) && !isNaN(this.amount))
      this.total = this.rate * this.amount;
  }

  public changeAmount() {
    // console.log('value ' + this.amount);
    if (!isNaN(this.rate) && !isNaN(this.amount))
      this.total = this.rate * this.amount;
  }

  public changeTotal() {
    // console.log('value ' + this.total);
    if (!isNaN(this.rate) && !isNaN(this.total))
      this.amount = this.total / this.rate;
  }

}
