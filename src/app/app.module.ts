import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import {AppComponent} from './app.component';

import {TickerComponent} from './ticker/ticker.component';
import {OrderComponent} from './order/order.component';
import {WidgetComponent} from './widget/widget.component';
import {TickerorderComponent} from './tickerorder/tickerorder.component';
import {KeysPipe} from './keys.pipe';
import {AppRoutingModule} from './app-routing.module';
import {ChartComponent} from './chart/chart.component';
import {CookieService} from 'ngx-cookie-service';
import {TradehistoryComponent} from './tradehistory/tradehistory.component';
import {TickerorderdetailComponent} from './tickerorderdetail/tickerorderdetail.component';
import {TradehistorynewComponent} from './tradehistorynew/tradehistorynew.component';
import {BasetableComponent} from './basetable/basetable.component';
import { TestComponent } from './test/test.component';
import {SearchFilterPipe} from './search.pipe';
import { TraidingComponent } from './traiding/traiding.component';

@NgModule({
  declarations: [
    AppComponent, BasetableComponent, TickerComponent, OrderComponent, WidgetComponent, TickerorderComponent, KeysPipe, SearchFilterPipe, ChartComponent, TradehistoryComponent, TickerorderdetailComponent, TradehistorynewComponent, TestComponent, TraidingComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, CommonModule, FormsModule, NgbModule.forRoot(), AppRoutingModule, ChartsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
