export class Order {

  public pair: string;
  public asks: OrderVals[];
  public bids: OrderVals[];
  public isFrozen: boolean;
  public seq: number;
}

export class OrderVals {

  public amount: number;
  public volume: number;
}
