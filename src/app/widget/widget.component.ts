import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {OrderVals} from './Order';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WidgetComponent implements OnInit {

  orders: OrderVals[];
  keys: string[];

  private timer: Observable<number>;


  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.timer = Observable.timer(5000, 500);
    this.timer.subscribe(t => this.getOrders());
  }

  getOrders() {
    /*this.http.get(environment.apiEndpoint + '/returnOrderBook/ALL/1', {responseType: 'json'})
      .subscribe(data => {
        this.keys = Object.keys(data);
        this.orders = Object.values(data);
        // console.log(this.orders);
      });*/
  }


}
