import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class CommonService {

  public sec = 1;
  public min = 60 * this.sec;
  public hour = 60 * this.min;
  public day = 24 * this.hour;
  public week = 7 * this.day;


  constructor(private cookie: CookieService) {
  }

  public getCookie<T>(key: string, defaultValue: T): T {
    const value = this.cookie.get(key);
    return (isNullOrUndefined(value) ? defaultValue : JSON.parse(value) as T);
  }

  public putCookie<T>(key: string, value: T) {
    this.cookie.set(key, value.toString());
  }

  public getHoursAgo(hours: number): number {
    return Math.floor(Date.now() / 1000) - hours * this.hour;
  }

  public getMinutesAgo(minutes: number): number {
    return Math.floor(Date.now() / 1000) - minutes * this.min;
  }

  public getSecondsAgo(seconds: number): number {
    return Math.floor(Date.now() / 1000) - seconds * this.sec;
  }

  /**
   * @param pair Pair.
   * @param type Bid - 0, Ask - 1.
   * @param price Price.
   */
  public getPriceInUSDT(pair: string, type: boolean, price: number, btc: any, eth: any, xmr: any, usdc: any): number {
    if (isNullOrUndefined(btc)) {
      return -1;
    }
    let rate: number;
    switch (pair.substring(0, pair.indexOf('_'))) {
      case 'BTC':
        rate = (type ? btc.ask : btc.bid);
        break;
      case 'ETH':
        rate = (type ? eth.ask : eth.bid);
        break;
      case 'XMR':
        rate = (type ? xmr.ask : xmr.bid);
        break;
      case 'USDT':
        rate = 1;
        break;
      case 'USDC':
        rate = (type ? usdc.ask : usdc.bid);
        break;
    }
    // console.log(btc);
    return rate * price;
  }

  public unsubscribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  public startPolling(project: (value: any, index: number) => Observable<Object>, sleep: number, value: any): Subscription {
    return Observable.interval(sleep)
      .concatMap(project)
      .subscribe((data) => {
        value = data;
        console.log(data);
      });
  }

}
