import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {StockService} from '../stock.service';
import {CommonService} from '../common.service';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-tradehistorynew',
  templateUrl: './tradehistorynew.component.html',
  styleUrls: ['./tradehistorynew.component.css'],
  providers: [StockService, CommonService]
})
export class TradehistorynewComponent implements OnInit, OnDestroy {

  @Input() pair: string;
  @Input() limit: number = null;
  @Input() time: number = null; // seconds

  public maxValue: number;
  public minValue: number;

  public sellVol: number;
  public buyVol: number;
  public sellTotal: number;
  public buyTotal: number;

  private sub: Subscription;
  public parts = [2000, 5000, 7000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 55000, 60000, 120000, 180000, 240000]; // milliseconds

  public columns = ['#', 'Sec', 'Min', 'Max', 'Volume', 'Total', 'Total volume', 'Total price', 'Cnt',
    'Min', 'Max', 'Volume', 'Total', 'Total volume', 'Total price', 'Cnt'];
  public tradehistory: any;


  constructor(private stockService: StockService, private common: CommonService) {
    this.pair = 'USDT_BTC';
    this.time = 300;
    this.limit = 1000;
    this.maxValue = this.stockService.maxValue;
    this.minValue = this.stockService.minValue;
    this.sellVol = this.buyVol = this.sellTotal = this.buyTotal = 0;
  }

  ngOnInit() {
    /* if (this.startDate === null) {
      this.startDate = this.common.getSecondsAgo(this.time);
    }
    if (this.limit === null) {
      this.limit = 1000;
    } */
    this.startPolling();
  }

  ngOnDestroy() {
    this.endPolling();
  }

  private startPolling() {
    this.sub = Observable.interval(500)
      .concatMap(() => this.stockService.getStatisticTradeHistory2(this.pair,
        this.common.getSecondsAgo(this.time), 9999999999, this.limit, this.parts))
      .subscribe((data) => {
        this.tradehistory = data;
        this.sellVol = this.buyVol = this.sellTotal = this.buyTotal = 0;
        // console.log(data);
      });
  }

  private endPolling() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  public getTotal(isSell, isVolume: boolean, index: number): number {
    if (isNullOrUndefined(this.tradehistory)) {
      return 0;
    }
    let value = 0;
    if (isVolume) {
      for (const i in this.tradehistory) {
        // console.log(this.tradehistory[i])
        value += (isSell ? this.tradehistory[i].sell.amount : this.tradehistory[i].buy.amount);
        if (Number(i) >= index) {
          break;
        }
      }
    } else {
      for (const i in this.tradehistory) {
        value += (isSell ? this.tradehistory[i].sell.total : this.tradehistory[i].buy.total);
        if (Number(i) >= index) {
          break;
        }
      }
    }
    return value;
  }

}
