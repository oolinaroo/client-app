import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradehistorynewComponent } from './tradehistorynew.component';

describe('TradehistorynewComponent', () => {
  let component: TradehistorynewComponent;
  let fixture: ComponentFixture<TradehistorynewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradehistorynewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradehistorynewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
