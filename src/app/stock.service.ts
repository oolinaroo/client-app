import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class StockService {

  const
  maxValue = Number.MAX_SAFE_INTEGER;
  minValue = Number.MIN_SAFE_INTEGER;

  constructor(private http: HttpClient) {
  }

  private modifySell1(value: any) {
    const op = value.type === 'buy';
    return {
      min: (op ? Number(value.rate) : Number.MAX_SAFE_INTEGER),
      max: (op ? Number(value.rate) : Number.MIN_SAFE_INTEGER),
      amount: (op ? Number(value.total) : 0),
      count: (op ? 1 : 0)
    };
  }

  private modifyBuy1(value: any) {
    const op = value.type === 'sell';
    return {
      min: (op ? Number(value.rate) : Number.MAX_SAFE_INTEGER),
      max: (op ? Number(value.rate) : Number.MIN_SAFE_INTEGER),
      amount: (op ? Number(value.total) : 0),
      count: (op ? 1 : 0)
    };
  }

  private modify2(i: number, name: string, value: any) {
    return {
      min: (value.part === i ? value[name].min : Number.MAX_SAFE_INTEGER),
      max: (value.part === i ? value[name].max : Number.MIN_SAFE_INTEGER),
      amount: (value.part === i ? value[name].amount : 0),
      count: (value.part === i ? value[name].count : 0)
    };
  }

  private modify3(i: number, name: string, prev: any, curr: any) {
    return {
      amount: prev[`${name}${i}`].amount + curr[`${name}${i}`].amount,
      count: prev[`${name}${i}`].count + curr[`${name}${i}`].count,
      min: (prev[`${name}${i}`].min < curr[`${name}${i}`].min ? prev[`${name}${i}`].min : curr[`${name}${i}`].min),
      max: (prev[`${name}${i}`].max > curr[`${name}${i}`].max ? prev[`${name}${i}`].max : curr[`${name}${i}`].max)
    };
  }

  getOrderBook(pair: string, depth: number) {
    return this.http.get(`${environment.apiEndpoint}/returnOrderBook?pair=${pair}&depth=${depth}`);
  }

  getChartData(pair: string, start: number = null, period: number = 300) {
    return this.http.get(`${environment.apiEndpoint}/returnChartData?pair=${pair}&period=${period}&start=${start}&end=9999999999`);
  }

  getTradeHistory(pair: string, start: number = null, end = null, limit: number = 100) {
    return this.http.get(`${environment.apiEndpoint}/returnTradeHistory?pair=${pair}&start=${start}&end=${end}&limit=${limit}`);
  }

  getStatisticTradeHistory(pair: string, start: number = null, end = null, limit: number = 100, parts: any[]): Observable<any> {
    const data = {
      sell0: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy0: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell1: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy1: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell2: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy2: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell3: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy3: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell4: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy4: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell5: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy5: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell6: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy6: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      sell7: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0},
      buy7: {min: Number.MAX_SAFE_INTEGER, max: Number.MIN_SAFE_INTEGER, amount: 0, count: 0}
    };
    let last, delta, index;
    return this.getTradeHistory(pair, start, end, limit)
      .map((values: any) => {
        last = new Date(values[0].date);
        return values.map((value: any) => {
          delta = +last - +(new Date(value.date));
          for (const i in parts) {
            if (delta <= parts[i]) {
              index = i;
              break;
            }
          }
          return {
            sell: this.modifySell1(value),
            buy: this.modifyBuy1(value),
            date: value.date,
            diff: delta,
            part: Number(index)
          }
            ;
        }).map((value: any) => {
          // console.log(value);
          return {
            sell0: this.modify2(0, 'sell', value),
            buy0: this.modify2(0, 'buy', value),
            sell1: this.modify2(1, 'sell', value),
            buy1: this.modify2(1, 'buy', value),
            sell2: this.modify2(2, 'sell', value),
            buy2: this.modify2(2, 'buy', value),
            sell3: this.modify2(3, 'sell', value),
            buy3: this.modify2(3, 'buy', value),
            sell4: this.modify2(4, 'sell', value),
            buy4: this.modify2(4, 'buy', value),
            sell5: this.modify2(5, 'sell', value),
            buy5: this.modify2(5, 'buy', value),
            sell6: this.modify2(6, 'sell', value),
            buy6: this.modify2(6, 'buy', value),
            sell7: this.modify2(7, 'sell', value),
            buy7: this.modify2(7, 'buy', value)
          };
        }).reduce((prev: any, curr: any) => {
          // console.log(prev);
          // console.log(curr);
          return {
            sell0: this.modify3(0, 'sell', prev, curr),
            buy0: this.modify3(0, 'buy', prev, curr),
            sell1: this.modify3(1, 'sell', prev, curr),
            buy1: this.modify3(1, 'buy', prev, curr),
            sell2: this.modify3(2, 'sell', prev, curr),
            buy2: this.modify3(2, 'buy', prev, curr),
            sell3: this.modify3(3, 'sell', prev, curr),
            buy3: this.modify3(3, 'buy', prev, curr),
            sell4: this.modify3(4, 'sell', prev, curr),
            buy4: this.modify3(4, 'buy', prev, curr),
            sell5: this.modify3(5, 'sell', prev, curr),
            buy5: this.modify3(5, 'buy', prev, curr),
            sell6: this.modify3(6, 'sell', prev, curr),
            buy6: this.modify3(6, 'buy', prev, curr),
            sell7: this.modify3(7, 'sell', prev, curr),
            buy7: this.modify3(7, 'buy', prev, curr)
          };
        }, data);
      });
  }

  /* getDetailedStatisticTradeHistory(pair: string, start: number = null, end = null, limit: number = 100): Observable<any> {
    const data = {sell: {amount: 0, count: 0}, buy: {amount: 0, count: 0}};
    return this.getTradeHistory(pair, start, end, limit)
      .map((values: any) => {
        return values.map((value: any) => {
          return {
            sell: {amount: (value.type === 'sell' ? Number(value.total) : 0), count: (value.type === 'sell' ? 1 : 0)},
            buy: {amount: (value.type === 'buy' ? Number(value.total) : 0), count: (value.type === 'buy' ? 1 : 0)}
          };
        }).reduce((prev: any, curr: any) => {
          return {
            sell: {amount: prev.sell.amount + curr.sell.amount, count: prev.sell.count + curr.sell.count},
            buy: {amount: prev.buy.amount + curr.buy.amount, count: prev.buy.count + curr.buy.count}
          };
        }, data);
      });
  } */

  getStatisticTradeHistory2(pair: string, start: number = null, end = null, limit: number = 100, parts: any[]): Observable<any> {
    let last, i, tmp, delta, isSell;
    const res: any[] = [];
    parts.forEach((part: any) => {
      res.push({
        part: part, sell: {
          min: this.maxValue, max: this.minValue,  // Rate
          amount: 0, total: 0, count: 0
        },
        buy: {
          min: this.maxValue, max: this.minValue,  // Rate
          amount: 0, total: 0, count: 0
        }
      });
    });
    // console.log(res);
    return this.getTradeHistory(pair, start, end, limit)
      .map((values: any) => {
        last = new Date(values[0].date);
        values.forEach((value: any) => {
          for (const j in parts) {
            delta = +last - +(new Date(value.date));
            if (delta <= parts[j]) {
              i = j;
              break;
            }
          }
          tmp = res[i];
          isSell = (value.type === 'sell' ? true : false);
          res[i] = {
            part: tmp.part,
            sell: {
              min: (isSell ? (tmp.sell.min > Number(value.rate) ? Number(value.rate) : tmp.sell.min) : tmp.sell.min),
              max: (isSell ? (tmp.sell.max < Number(value.rate) ? Number(value.rate) : tmp.sell.max) : tmp.sell.max),
              amount: (isSell ? tmp.sell.amount + Number(value.amount) : tmp.sell.amount),
              total: (isSell ? tmp.sell.total + Number(value.total) : tmp.sell.total),
              count: (isSell ? tmp.sell.count + 1 : tmp.sell.count)
            },
            buy: {
              min: (!isSell ? (tmp.buy.min > Number(value.rate) ? Number(value.rate) : tmp.buy.min) : tmp.buy.min),
              max: (!isSell ? (tmp.buy.max < Number(value.rate) ? Number(value.rate) : tmp.buy.max) : tmp.buy.max),
              amount: (!isSell ? tmp.buy.amount + Number(value.amount) : tmp.buy.amount),
              total: (!isSell ? tmp.buy.total + Number(value.total) : tmp.buy.total),
              count: (!isSell ? tmp.buy.count + 1 : tmp.buy.count)
            },
          };
          // console.log(res[i]);
          // return value;
        });
        return res;
      });
  }

  getStatisticOrderBook(pair: string, depth: number) {
    return this.http.get(`${environment.apiEndpoint}/returnOrderBook?pair=${pair}&depth=${depth}`)
      .map((values: any) => {
        let data, asks, bids, order, lastPrice,
          lastVolume;
        const res: any[] = [];
        for (const key in values) {
          data = values[key];
          // Use `key` and `value`
          // console.log(data);
          for (const type of ['asks', 'bids']) {
            // console.log(type);
            lastPrice = (data.isFrozen === '1' ? 0 : Number(data[type][0][0]));
            lastVolume = (data.isFrozen === '1' ? 0 : Number(data[type][0][1]));
            order = {totalVolume: 0, totalPrice: 0};
            data[type].forEach((value: any) => {
              order = {
                totalVolume: order.totalVolume + value[1],
                totalPrice: order.totalPrice + Number(value[0]) * value[1]
              };
            });
            // console.log(order);
            if (type === 'asks') {
              asks = Object.assign({lastPrice: lastPrice, lastVolume: lastVolume}, order);
              // console.log(asks);
            } else {
              bids = Object.assign({lastPrice: lastPrice, lastVolume: lastVolume}, order);
              // console.log(bids);
            }
          }
          res.push(Object.assign({key: key, asks: asks, bids: bids}, {isFrozen: data.isFrozen, seq: data.seq}));
        }
        return res;
      });
  }

}
